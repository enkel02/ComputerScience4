#include <iostream>
#include "DNode.h"

// TODO: Написать функцию NodeListing(PNode &Head, PNode &Tail, int start_begin, (void) f(PNode p)),
// которая применяет функцию f на каждый элемент двусвязного списка, порядок движения которого зависит от start_begin
// START_BEGIN - ход с головы списка
// START_END - ход с конца списка

int main() {
    /*
     * Функции, который были вызваны в тесте:
     * - CreateNode     +
     * - AddFirst       +
     * - AddLast        +
     * - AddAfter       +
     * - AddBefore      +
     * - Find           +
     * - DeleteNode     +
     * - NodeListing    - (Not implemented yet)
     */
    std::cout << "Reading DList:" << std::endl;

    // Testing: CreateNode, AddAfter, AddFirst
    PNode d1 = CreateNode("Hello");
    PNode d2 = CreateNode("my");
    PNode d3 = CreateNode("dear");
    PNode d4 = CreateNode("friends!!");
    Head = NULL;
    Tail = NULL;
    AddFirst(Head, Tail, d1);
    AddAfter(Head, Tail, d1, d2);
    AddAfter(Head, Tail, d2, d3);
    AddAfter(Head, Tail, d3, d4);

    // Testing: AddLast
    PNode d5 = CreateNode("and reviewers!!");
    AddLast(Head, Tail, d5);

    // Testing: Find
    PNode d6 = Find(Head, Tail, START_BEGIN, "dear");       // Не забывайте проверить возвратил ли Find NULL или нет.
    strcpy(d6->word, "dearest");                            // Иначе в таких случаях программа падает. =)

    // Testing: AddBefore
    PNode d7 = CreateNode("guests!!");
    AddBefore(Head, Tail, d5, d7);

    // Testing: Delete
    PNode d8 = CreateNode("This string will be deleted");
    AddAfter(Head, Tail, d3, d8);
    Delete(Head, Tail, d8);

    PNode p = Head;
    while(p != NULL) {
        cout << p->word << endl;
        p = p->next;
    }
    return 0;
}