//
// Created by work on 22.03.17.
//

#ifndef DNODE_DNODE_H
#define DNODE_DNODE_H

#include <iostream>
#include <cstring>
using namespace std;

struct Node {
    char word[40];      // Область данных
    int count;
    Node *next, *prev;  // Ссылки на соседние узлы
};

typedef Node * PNode;   // Создаем новый тип данных <<Указатель на узел>>
PNode Head = NULL;      // Ссылка на первый элемент
PNode Tail = NULL;      // Ссылка на последний элемент

// Создание узла двусвязного списка
PNode CreateNode(char NewWord[]) {
    /*
     *  Создает новый узел в памяти с данными NewWord и возвращает его адрес. Обратите внимание, что при записи  узел
     *  используется обращение к полям структуры через указатель.
     */

    PNode NewNode = new Node;   // Указатель на новый узел
    strcpy(NewNode->word, NewWord);     // Запись слова
    NewNode->count  = 1;        // Задаем счетчик
    NewNode->next = NULL;       // Следующего узла нет
    NewNode->prev = NULL;       // Предыдущего узла также нет =)
    return NewNode;
}

// Добавление узла в начало списка
void AddFirst(PNode &Head, PNode &Tail, PNode NewNode) {
    NewNode->next = Head;
    NewNode->prev = NULL;
    if(Head) {              // Если Head является указателем на какой-то элемент
        Head->prev=NewNode;
    }
    Head = NewNode;
    if(!Tail) {             // Если Tail является NULL, то ссылки на последний элемент нет, т.е. список пуст,
        // Тогда, только что добавленный, элемент будет и Head-ом и Tail-ом
        Tail = Head;
    }
}

// Добавление узла в начало списка
void AddLast(PNode &Head, PNode &Tail, PNode NewNode) {
    NewNode->prev = Tail;
    NewNode->next = NULL;
    if(Tail) {
        Tail->next = NewNode;
    }
    Tail = NewNode;
    if(!Head) {
        Head = Tail;        // Аналогично из AddFirst
    }
}

#define START_BEGIN 1
#define START_END 0
// Поиск узла в списке
PNode Find(PNode &Head, PNode &Tail, bool start_begin, char NewWord[]) {
    /*
     * Данная функция использует аргумент start_begin, который может принимать значения:
     * START_BEGIN - поиск начиная с Head
     * START_END - поиск начиная с конца(Tail)
     *
     * Пример вызова функции:
     * Find(Head, Tail, START_BEGIN, "Nail");
     * Find(Head, Tail, START_END, "562");
     */

    if(start_begin) {   // Начинаем поиск с головы
        PNode q = Head;
        while((q != NULL) && strcmp(q->word, NewWord)) {
            q = q->next;
        }
        return q;
    } else {
        PNode q = Tail;
        while((q != NULL) && strcmp(q->word, NewWord)) {
            q = q->prev;
        }
        return q;
    }

}

// Добавление узла после заданного
void AddAfter(PNode &Head, PNode &Tail, PNode p, PNode NewNode) {
    /*
     * Алгоритм:
     * Дан адрес NewNode нового узла и адрес p одного из существующих в списке.
     * Требуется вставить в список новый узел после p.
     * Если узел p является последним, то операция сводится к добавлению в конец списка.
     * Если узел p не последний, то операция вставки выполняется в два этапа:
     * 1) Установить ссылки нового узла на следующий за данным(next) и предществующий ему (prev)
     * 2) Установить ссылки соседних узлов так, чтобы включить NewNode в список.
     *
     */
    if(!p->next) {
        AddLast(Head, Tail, NewNode);   // Вставка в конец списка
    } else {
        NewNode->next = p->next;        // Меняем ссылки нового узла
        NewNode->prev = p;
        p->next->prev = NewNode;        // Меняем ссылки соседних узлов
        p->next = NewNode;
    }
}

// Добавление узла перед заданным
void AddBefore(PNode &Head, PNode &Tail, PNode p, PNode NewNode) {
    /*
     * Алгоритм аналогичный AddAfter
     */
    if(!p->prev) {
        AddFirst(Head, Tail, NewNode);
    } else {
        NewNode->next = p;
        NewNode->prev = p->prev;
        p->prev->next = NewNode;
        p->prev = NewNode;
    }

}

// Удаление узла
void Delete(PNode &Head, PNode &Tail, PNode OldNode) {
    /*
     * Эта процедура также требует ссылки на голову и хвост списка, поскольку они могут измениться при удалении
     * крайнего элемента списка. На первом этапе устанавливаются ссылки соседних узлов(если они есть) так, как если
     * бы удаляемого узла не было бы. Затем узел удаляется и память, которую он занимает, освобождается.
     * Отдельно проверяется, не является ли удаляемый узел перым или последним узлом списка.
     */
    if(Head == OldNode) {
        Head = OldNode->next;   // Удаляем первый элемент

        if (Head) {             // Если Head указывает на какой-то Node
            Head->prev = NULL;
        } else {
            Tail = NULL;        // Удалили единственный элемент
        }
    } else {
        OldNode->prev->next = OldNode->next;
        if(OldNode->next) {
            OldNode->next->prev = OldNode->prev;
        } else {
            Tail = NULL;
        }
    }
    delete OldNode;

}
#endif //DNODE_DNODE_H
