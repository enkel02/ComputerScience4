#include <iostream>


/*
 * Стек есть структура типа Last in -First Out - упорядоченный набор элементов, в котором добавление новых и удаление
 * существующих элементов допустимо только с одного конца - вершины стека. Стек имеет 2 функции "Вставить элемент (Push)
 * и Удалить элемент(Pop)
 */

// Объявление элемента стека
const int MAXSIZE = 100;
struct Stack{
    char data [MAXSIZE];
    int size;
};

typedef Stack * S;

// Вставка элемента
void Push(Stack &S, char x) {
    if (S.size == MAXSIZE) {
        printf("Стек переполнен");
        return;
    }
    S.data[S.size] = x;
    S.size++;
}

// Удаление элемента
char Pop(Stack &S) {
    if(S.size == 0) {
        printf("Стек пуст");
        return char(255); // Этот символ никогда не может находиться в стеке по условию задачи
    }
    S.size--;
    return S.data[S.size];
}

/*
 * Решить задачу: ввести символьную строку, которая может содержать три вида скобок: (), [], {}. Определить, верно
 * ли расставлены скобки (символы между скобками не учитывать). Например, в строках ()[{}] и [{}([])] скобки расстаылены
 * верно, а в строках ([)) и ]]]((( - не верно
 */
int main() {
    char br1[3] = {'(', '[', '{'}; // открывающие скобки
    char br2[3] = {')', ']', '}'}; // закрывающие скобки
    char s[80], upper;
    int i, k, OK;
    Stack S; // стек символов
    printf("Введите выражение со скобками: ");
    gets(s);
    S.size = 0; // Сначала стек пуст
    OK = 1;
    for(i = 0; OK && (s[i] != '\0'); i++)
        for(k = 0; k < 3; k++) {
            if(s[i] == br1[k]) {
                Push(S, s[i]);
                break;
            }
            if(s[i] == br2[k]) {
                upper = Pop(S);
                if(upper != br1[k])
                    OK = 0;
                break;
            }
        }
    if(OK && (S.size == 0))
        printf("\nВыражение правильное\n");
    else
        printf("\nВыражение не правильное\n");
    return 0;
}