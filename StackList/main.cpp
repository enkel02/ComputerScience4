#include <iostream>

struct Node {
    char data;
    Node *next, *prev;
};

typedef Node *PNode;
struct Stack {
    PNode Head, Tail;
};

// Вставка элемента
void Push(Stack &S, char x) {
    PNode  NewNode = new Node;  // Создание новго узла
    NewNode->data = x;          // заполнение данными
    NewNode->next = S.Head;
    NewNode->prev = NULL;
    if(S.Head) // Добавить в начало списка
        S.Head->prev = NewNode;
    S.Head = NewNode;
    if(!S.Tail)
        S.Tail = S.Head;
}

//  Получение верхнего элемента с вершины стека
char Pop(Stack &S)
{
    PNode TopNode = S.Head;
    char x;
    if(!TopNode)            // Стек пуст
        return char(255);
    x = TopNode->data;
    S.Head = TopNode->next;
    if(S.Head)
        S.Head ->prev = NULL;   // Переставить ссылки
    else
        S.Tail = NULL;
    delete TopNode;     // Освободить память
    return x;
}


int main() {
    char br1[3] = {'(', '[', '{'}; // открывающие скобки
    char br2[3] = {')', ']', '}'}; // закрывающие скобки
    char s[80], upper;
    int i, k, OK;
    Stack S; // стек символов
    printf("Введите выражение со скобками: ");
    gets(s);
    S.Head = NULL; // Сначала стек пуст
    OK = 1;
    for(i = 0; OK && (s[i] != '\0'); i++)
        for(k = 0; k < 3; k++) {
            if(s[i] == br1[k]) {
                Push(S, s[i]);
                break;
            }
            if(s[i] == br2[k]) {
                upper = Pop(S);
                if(upper != br1[k])
                    OK = 0;
                break;
            }
        }
    if(OK && (!S.Head))
        printf("\nВыражение правильное\n");
    else
        printf("\nВыражение не правильное\n");
    return 0;
}