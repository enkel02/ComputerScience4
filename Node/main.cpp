#include <iostream>
#include "Node.h"



// Проход по списку
int main() {
    /*
     * TODO: Создать функцию NodeListing(PNode &Head, (void) f(PNode p)),
     * которая применяет функцию p, последовательно, к каждому элементу списка
     *
     * Функции, который были вызваны в тесте:
     * - CreateNode     +
     * - AddFirst       + (также вызывается в функции AddAfter)
     * - AddAfter       +
     * - AddBefore      +
     * - Find           +
     * - DeleteNode     +
     * - NodeListing    - (Not implemented yet)
     */
    std::cout << "Reading List:" << std::endl;

    // Testing: CreateNode, AddAfter
    Head = CreateNode("Hello");
    PNode n1 = CreateNode("my");
    PNode n2 = CreateNode("name");
    PNode n3 = CreateNode("is");
    PNode n4 = CreateNode("Nail");
    AddAfter(Head, n1);
    AddAfter(n1, n2);
    AddAfter(n2, n3);
    AddAfter(n3, n4);

    // Testing: AddFirst
    PNode first = CreateNode("Hey,");
    AddFirst(Head, first);

    // Testing: AddBefore
    PNode n5 = CreateNode("and surname");
    AddBefore(Head, n3, n5);

    // Testing: Find
    PNode nail = Find(Head, "Nail");
    strcpy(nail->word, "Iskhakov Nail");

    // Testing: DeleteNode
    PNode del = CreateNode("I want to delete this Node");
    AddAfter(n3, del);
    DeleteNode(Head, del);

    PNode p = Head;
    while(p != NULL) {
        cout << p->word << endl;
        p = p->next;
    }
    return 0;
}