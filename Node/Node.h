//
// Created by work on 22.03.17.
//

#ifndef NODE_NODE_H
#define NODE_NODE_H

#include <iostream>
#include <cstring>
using namespace std;

struct Node {
    char word[40];  // Область данных
    int count;
    Node *next;     // Ссылка на следующи элемент
};

typedef Node *PNode;    // Создаем новый тип данных: указатель на узел
PNode Head = NULL;

// Создание элемента списка
PNode CreateNode(char NewWord[]) {
    /*
     *  Создает новый узел в памяти с данными NewWord и возвращает его адрес. Обратите внимание, что при записи  узел
     *  используется обращение к полям структуры через указатель.
     */

    PNode NewNode = new Node;   // Указатель на новый узел
    strcpy(NewNode->word, NewWord);     // Запись слова
    NewNode->count  = 1;        // Задаем счетчик
    NewNode->next = NULL;       // Следующего узла нет
    return NewNode;
}

// Добавление узла в начало списка
void AddFirst(PNode &Head, PNode NewNode) {
    NewNode->next = Head;
    Head = NewNode;
}

// Добаление узла после заданного
void AddAfter(PNode p, PNode NewNode) {
    NewNode->next = p->next;
    p->next = NewNode;
}

// Добавление узла перед заданным
void AddBefore(PNode &Head, PNode p, PNode NewNode) {
    /*
     * В односвязном списке, связи напрвалены только в одну сторону и для того, чтобы получить адрес предыдущего узла,
     * нужно пройти весь списко сначала. Затем задача седется либо к вставке узла в начало списка (если заданный узел -
     * первый), либо к вставке после заданного узла.
     */
    PNode q = Head;
    if(Head == p) {
        AddFirst(Head, NewNode);    // Вставка перед первым узлом
        return;
    }

    while((q != NULL) && (q->next != p)) {  // Ищем узел, за которым следует p
        q = q->next;
    }
    if(q != NULL) { // Если такой узел нашли
        AddAfter(q, NewNode);
    }

    // В данном случае, если элемент p не найден, то q равен NULL и ничего не происходит
}


// Поиск узла в списке
PNode Find(PNode Head, char NewWord[]) {
    /*
     * Алгоритм:
     * 1) Начать с головы списка
     * 2) Пока текущий элемент существует (указатель не NULL), проверить нужное условие перейти к следующему элементу
     * 3) Закончить, когда требуемый элемент найден или все элементы просмотрены
     */
    PNode q = Head;
    while((q != NULL) && strcmp(q->word, NewWord)) {
        q = q->next;
    }
    return q;
}

// Удаление узла
void DeleteNode(PNode &Head, PNode OldNode) {
    PNode q = Head;
    if(Head == OldNode) {
        Head = OldNode->next;
    } else {
        while((q != NULL) && (q->next != OldNode)) {
            q = q->next;
        }
        if(q == NULL) {
            return;
        }
        q->next = OldNode->next;
    }
    delete OldNode;
}

#endif //NODE_NODE_H
